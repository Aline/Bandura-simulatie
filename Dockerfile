FROM node:current

WORKDIR /usr/app

RUN npm install --global ntypescript typescript; npm install terminal-kit; npm install random-seed; npm install csv-writer

RUN npm i --save-dev @types/node

COPY runsimulation.sh /usr/app/runsimulation.sh

COPY bandura.ts /usr/app/bandura.ts

RUN cd /usr/app/ && tsc -t es5 bandura.ts

CMD cd /usr/app/ && ./runsimulation.sh
