#! /bin/sh
# Read CSV and run the defined simulations
INPUT=out/runs.csv
IFS=','
re='^[a-Z]+$'
Nsimulations=$(wc -l < $INPUT)
i=1
[ ! -f $INPUT ] && { echo "$INPUT file not found"; exit 99; }
while read c minab maxab mins maxs mineab maxeab vpmax efmax
do
    case $c in
        ''|*[!0-9]*) ;;
        *) node bandura.js -out /result/run_${i}c${c}_abmin${minab}_abmax${maxab}_smin${mins}_smax${maxs}_pmax${vpmax}_effort${efmax} -amin ${minab} -amax ${maxab} -smin ${mins} -smax ${maxs} -agents ${c} -emin ${mineab} -emax ${maxeab} -pmax ${vpmax} -efmax ${efmax};
        i=$((i+1))
        echo "$i out of $Nsimulations done"
        ;;
    esac
done < $INPUT
IFS=$OLDIFS
chown -R 1000:1000 ./out/result/
