# Bandura simulation
Script as given for the bachelors assignment with a few fixes, and tooling to run for different parameters.

## Running

First download the repository:
```
git clone https://git.vgorcum.com/mathijs/Bandura.git
```
Open the directory, `cd Bandura`

We must first build the docker container (this is where the typescript file is put in the container and compiled, so changes to bandura.ts requires a rebuild of the container).

```
docker build -t bandura .
```

To run we need to map a volume to `/usr/app/out` which contains the `runs.csv` file, in which we define the parameters for each run. The result of the simulation will then be placed in the `result/` folder in said mapped volume.

In this example we'll just map the current directory to `/usr/app/out`.

```
docker run --rm -v $PWD:/usr/app/out bandura
```

This will read the `runs.csv`, and run a simulation for each line with the values set for amin, amax, smin, smax, and number of agents in the `runs.csv` file.
