import { terminal as term } from 'terminal-kit';
const rand = require('random-seed').create();
const createCsvWriter = require('csv-writer').createArrayCsvWriter;
const fs = require('fs');

interface itask {
    id: number;
    difficulty: number;
}

interface itask_result {
    result: boolean;
    strained: boolean; // When result is success, but effort spent was more than effort resiliency
}

interface ihistory extends itask, itask_result {
    ability_est: number;
    mood: number;
    effort: number;
    efficacy: number;
}

let args = process.argv.slice(2);
let ia = 0;
let unrecognizedParam: boolean = false;
let arg_ability_min: number = undefined;
let arg_ability_max: number = undefined;
let arg_similarity_min: number = undefined;
let arg_similarity_max: number = undefined;
let arg_ability_est_min: number = undefined;
let arg_ability_est_max: number = undefined;
let arg_persuasion_max: number = undefined;
let arg_agents: number = undefined;
let arg_outputfolder: string = undefined;
let arg_effort_max: number = undefined;

while (ia < args.length) {
    switch (args[ia]) {
        case '-out':
            ++ia;
            arg_outputfolder = args[ia].toLowerCase();
        break;
        case '-amin':
            ++ia;
            arg_ability_min = Number(args[ia]);
        break;
        case '-amax':
            ++ia;
            arg_ability_max = Number(args[ia]);
        break;
        case '-smin':
            ++ia;
            arg_similarity_min = Number(args[ia]);
        break;
        case '-smax':
            ++ia;
            arg_similarity_max = Number(args[ia]);
        break;
        case '-emin':
            ++ia;
            arg_ability_est_min = Number(args[ia]);
        break;
        case '-emax':
            ++ia;
            arg_ability_est_max = Number(args[ia]);
        break;
        case '-pmax':
            ++ia;
            arg_persuasion_max = Number(args[ia]);
        break;
        case '-agents':
            ++ia;
            arg_agents = Number(args[ia]);
        break;
        case '-efmax':
            ++ia;
            arg_effort_max = Number(args[ia]);
        break;
        default:
            term.red(`Unrecognized argument: ${args[ia]}.\n`);
            unrecognizedParam = true;
        break;
    }
    ++ia;
}
if (unrecognizedParam) throw new Error("Unrecognized parameter(s) passed. Exiting Bandura...");

let OUTPUT_FOLDER = `./out` + (arg_outputfolder ?? '/test');
let SIMULATION_OUTPUT_PATH = `${OUTPUT_FOLDER}/simulation.csv`;
let SIMILARITIES_OUTPUT_PATH = `${OUTPUT_FOLDER}/similarities.csv`;
let ABILITIES_OUTPUT_PATH = `${OUTPUT_FOLDER}/abilities.csv`;
let DEFAULT_EFFORT_MAX = 100;
let ABILITY_MIN = 400;
let ABILITY_MAX = 2800;
let SCORE_CONSTANT = 400;
let SCORE_MULTIPLIER_BEGINNER = 32;
let SCORE_MULTIPLIER_MASTER = 12;
let EFFORT_RESILIENCY_TRESHOLD = 0.11920292202211755; // Based on chance of success when difficulty > two standard deviations than true ability
let MOOD_EFFECT_ON_EFFORT_RESILIENCY = .01; // Each positive mood point adds this value (can be negative!) to the effort-resiliency-treshold
let MOOD_MIN = -50; // Arbitrary limit on negative mood
let MOOD_MAX = 50; // Arbitrary limit on positive mood
let SIMILARITY_MIN = 0;
let SIMILARITY_MAX = 1;
let AGENT_COUNT = arg_agents ?? 10;
let TASK_COUNT = 2000;
let RANDOM_ABILITY_MIN = arg_ability_min ?? ABILITY_MIN;
let RANDOM_ABILITY_MAX = arg_ability_max ?? ABILITY_MAX;
let RANDOM_ABILITY_EST_MIN = arg_ability_est_min ?? ABILITY_MIN;
let RANDOM_ABILITY_EST_MAX = arg_ability_est_max ?? ABILITY_MAX;
let RANDOM_DIFFICULTY_MIN = ABILITY_MIN;
let RANDOM_DIFFICULTY_MAX = ABILITY_MAX;
let RANDOM_SIMILARITY_MIN = arg_similarity_min ?? SIMILARITY_MIN;
let RANDOM_SIMILARITY_MAX = arg_similarity_max ?? SIMILARITY_MAX;
let TASK_DIFFICULTY_MIN = ABILITY_MIN;
let TASK_DIFFICULTY_MAX = ABILITY_MAX;
let PERSUASION_MAX = arg_persuasion_max ?? 25;
let EFFORT_MAX = arg_effort_max ?? DEFAULT_EFFORT_MAX;
let MOOD_EFFORT_EFFECT = 0.25;
let TASK_RESULT_EFFORT_EFFECT = 1;
// mith (met ability=0) voerde taak uit met d=2.
// Kans op success = 11.920292202211755

let verbal_persuasion_min_bonus = 0; // Rol modifier
let verbal_persuasion_max_bonus = 1; // Rol modifier

let rolldie = () => Math.random();
let bound_to_range = (x: number, min: number, max: number) => {
    if (x <= min) return min;
    if (x >= max) return max;

    return x;
}

let bound_to_ability_range = (x: number): number => bound_to_range(x, ABILITY_MIN, ABILITY_MAX);

let bound_to_ability_est_range = (x: number): number => bound_to_range(x, Math.floor(ABILITY_MIN*0.9), Math.floor(ABILITY_MAX*1.1));

let get_random_in_range = (min: number, max: number): number => rand.floatBetween(min, max);
let get_random_persuasion_bonus = (): number => get_random_in_range(verbal_persuasion_min_bonus, verbal_persuasion_max_bonus);

function expected_score(difficulty: number, ability: number): number {
    return 1 / (1 + Math.pow(10, (difficulty - ability) / SCORE_CONSTANT));
}

function normalized_similarity(similarity_char: number, similarity_abi: number): number {
    let euclidean_similarity = Math.sqrt(Math.pow(similarity_char, 2) + Math.pow(similarity_abi, 2));
    let normalized_similarity = euclidean_similarity / Math.sqrt(2);

    return normalized_similarity;
}


function derivative_expected_score(task: number, ability: number): number { // derivate of logistics function used above, somewhat similar to a gaussian curve, normalized to 1
    return (Math.pow(2,-4 + (task - ability)/400)* Math.pow(5,-2 + (task - ability)/400) * 2.302585092994046)/Math.pow(1 + Math.pow(10,(task - ability)/400),2)/0.00144;
}

interface IAgentHash {
    [agent_id: string]: number;
}
class agent {
    public id: string;
    // Ability score range = [-3,+3]
    protected _ability_true: number;
    public get ability_true() {
        return this._ability_true;
    }

    public set ability_true(x: number) {
        this._ability_true = bound_to_ability_range(x);
    }

    protected _ability_est: number; // = self_efficacy_global (not mood-affected)
    public get ability_est() {
        return this._ability_est;
    }

    public set ability_est(x: number) {
        this._ability_est = bound_to_ability_est_range(x);
    }
    
    public SelfEfficacy: number;
    
    public updateEfficacy(){
        this.SelfEfficacy=(this._ability_est-this.ability_true)/this.ability_true;
    }
    
    public _effort: number;

    public setEffort(x: number){
        if (Math.abs(x) > EFFORT_MAX) { //limit the effort to its minimum or maximum, +/-100 by default
            x=Math.sign(x)*EFFORT_MAX
        }
        this._effort = x;
    }

    public get effort() {
        return this._effort;
    }

    public starting_ability_est: number; // For reporting only

    public history: Array<ihistory>;
    protected _result_last_performed_task: boolean;
    protected _spent_too_much_effort_last_performed_task: boolean;

    public personal_chars: number;
    public feedback_roll_modifier: number; // Modifier for determining success of next task
    public temperament: number; // Determines mood change per task success/fail
    public base_mood: number; // Starting mood modifier
    public get mood(): number { // Current mood modifier
        let result2num = (result: boolean, strained: boolean): number => (result && !strained) ? this.temperament : -this.temperament;
        let mood_modifier = 0;
        this.history.forEach(result => { mood_modifier += result2num(result.result, result.strained); });
        return bound_to_range(this.base_mood + mood_modifier, MOOD_MIN, MOOD_MAX);
    }
    protected get effort_resiliency(): number {
        return EFFORT_RESILIENCY_TRESHOLD - (this.mood * MOOD_EFFECT_ON_EFFORT_RESILIENCY);
    }

    public is_task_straining(difficulty: number): boolean {
        /* Effort resiliency is stated as follows:
         * When someone has a lower than a given probability to succeed at a task, then
         * the required effort to succeed at that task is beyond the effort resiliency range (threat appraisal).
         * However, mood modifiers can increase or decrease the effort resiliency range.
         */
        return this.get_chance_of_success_based_on_true_ability(difficulty) < this.effort_resiliency;
    }

    public similarities_char: IAgentHash = {};
    public similarities_abi: IAgentHash = {};

    constructor(_id: string, _ability_true = 0, _ability_est = 0, _base_mood: number = 0, _temperament: number = 1, _effort: number = 0) {
        this.id = _id;
        this.history = new Array();
        this.feedback_roll_modifier = 0;
        this.base_mood = _base_mood;
        this.temperament = _temperament;
        this.ability_true = _ability_true;
        this.ability_est = this.starting_ability_est = _ability_est;
        this._effort = 0;
        this.SelfEfficacy=0;
    }

    public do_task(task: itask): boolean {
        let dieroll = rolldie();
        // Add feedback bonus to dieroll.
        // Note that this decreases the rolled value, because we determine the result of a task as roll <= expected score (i.e. lower = better)
        dieroll = dieroll;
        let task_result = this.determine_task_result(dieroll, task.difficulty);

        // term.grey.italic(`${this.id} (met ability=${this.ability_true}) voerde taak uit met d=${task.difficulty}.\nKans op success = ${this.get_chance_of_success_based_on_true_ability(task.difficulty)}.\n\tdieroll=${dieroll}\n\tResultaat=${task_result ? 'Geslaagd!' : 'Gefaald :-('}.\n`);

        this._result_last_performed_task = task_result;

        return task_result;
    }

    public determine_task_result(dieroll: number, difficulty: number): boolean {
        return dieroll <= expected_score(difficulty, this.ability_true+this.effort);
    }

    public get_chance_of_success_based_on_true_ability(difficulty: number): number {
        return expected_score(difficulty, this.ability_true);
    }

    public get_chance_of_success_based_on_est_ability(difficulty: number): number {
        return expected_score(difficulty, this.ability_est);
    }

    public get_result_of_last_performed_task(): boolean {
        return this._result_last_performed_task;
    }

    public get_spent_too_much_effort_last_performed_task(): boolean {
        return this._spent_too_much_effort_last_performed_task;
    }

    public get_similarity_to(agent_id: string): number {
        // Determine similarity distance
        let similarity_char = this.similarities_char[agent_id];
        let similarity_abi = this.similarities_abi[agent_id];

        let similarity = normalized_similarity(similarity_char, similarity_abi);
        return similarity;
    }

    public to_string(): string {
        return this.id;
    }
}

agent.prototype.toString = agent.prototype.to_string;

function write(_s: string): void {
    console.log(_s);
}

interface iagent_result {
    agent: agent;
    task: itask;
    result: boolean;
}

function take_turn(task: itask): void {
    let results = new Array<iagent_result>();

    /* Let all agents perform the task.
     * Determine result for the task per agent and store this in a list.
     */
    for (let a of agents) {
        let task_succeeded = a.do_task(task);
        results.push({ agent: a, task: task, result: task_succeeded });
    }

    /* Let all agents get feedback.
     */
    for (let a of agents) {
        a.feedback_roll_modifier = get_random_persuasion_bonus();
    }

    // N.B. Mood is already calculated by using task history and thus does not need manual update

    // Update self-efficacy
    for (let a of agents) {
        /* Self-efficacy is based on whether your last performed task was a success.
         * However, when you had to spent too much effort on the task, even though it was a success,
         * Bandura states that it negatively impacts your self-efficacy.
         * Here, we model that scenario as a task fail, as Bandura states that someone who spent to much
         * effort, thinks that he/she will be unable to succeed next time.
         */

        let result2num = (result: boolean): number => result ? 1 : 0;
        let expected_value_based_on_estimated_ability = a.get_chance_of_success_based_on_est_ability(task.difficulty);

        let task_result_as_number: number;
        // Now, determine whether the task is calculated as a success, based on the required effort and
        // the resiliency of the agent.
        let is_task_straining = a.is_task_straining(task.difficulty);
        if (is_task_straining) { // The task was outside the boundaries of the effort resiliency
            task_result_as_number = result2num(false); // Consider the task a failure, regardless of actual success
        }
        else {
            task_result_as_number = result2num(a.get_result_of_last_performed_task());
        }

        a.ability_est += SCORE_MULTIPLIER_BEGINNER * (task_result_as_number - expected_value_based_on_estimated_ability);
        // N.B. ability is already bound to range when updating ability_est (via set property)
        
        // Update effort based on task failure/success, modulo expected_value_based_on_estimated_ability, and based on current mood.
        // The maximum effect size of expected value is +-1 while mood is +-0.25 (by default). The verbal persuasion bonus is also linearly added; a random number between 0 and 0.25
        
        a.updateEfficacy()
        let Effort=derivative_expected_score(task.difficulty,a.ability_est)*EFFORT_MAX;
        a.setEffort(Effort*a.SelfEfficacy+a.feedback_roll_modifier*PERSUASION_MAX);
        
        
        // Update self-efficacy based on other agents
        for (let other of agents) {
            if (other == a) continue; // Don't consider yourself as "other"

            let expected_value_based_on_estimated_ability_other = other.get_chance_of_success_based_on_est_ability(task.difficulty);
            let task_result_as_number_other = result2num(other.get_result_of_last_performed_task());
            let normalized_similarity = a.get_similarity_to(other.id);
            a.ability_est += SCORE_MULTIPLIER_BEGINNER * (task_result_as_number_other - expected_value_based_on_estimated_ability_other) * normalized_similarity;
        }
        // Update agent history
        a.history.push(<ihistory>{ id: task.id, difficulty: task.difficulty, result: a.get_result_of_last_performed_task(), strained: is_task_straining, ability_est: a.ability_est, mood: a.mood, effort: a.effort, efficacy: a.SelfEfficacy});
    }
}

// Create output folder
if (!fs.existsSync(OUTPUT_FOLDER)) {
    fs.mkdirSync(OUTPUT_FOLDER, { recursive: true });
}

const seed = 'gillian seed';
rand.seed(seed);

let agents: Array<agent> = new Array<agent>();
let tasks: Array<itask> = new Array<itask>();
for (let i = 0; i < AGENT_COUNT; i++) {
    let ability_true = rand.intBetween(RANDOM_ABILITY_MIN, RANDOM_ABILITY_MAX);
    let ability_est = rand.intBetween(RANDOM_ABILITY_EST_MIN, RANDOM_ABILITY_EST_MAX);

    let a = new agent(`agent_${i}`, ability_true, ability_est);
    agents.push(a);
}

let task_difficulty = TASK_DIFFICULTY_MIN;
let floatdifficulty: number = TASK_DIFFICULTY_MIN;
let task_difficulty_delta: number = (TASK_DIFFICULTY_MAX - task_difficulty) / TASK_COUNT;
for (let i = 0; i < TASK_COUNT; i++) {
    tasks.push(<itask>{ id: i + 1, difficulty: task_difficulty });
    floatdifficulty= floatdifficulty + task_difficulty_delta;
    task_difficulty = Math.floor(floatdifficulty);
}

// Set random similarities for all agents
for (let a of agents) {
    for (let b of agents) {
        if (a == b) continue;
        if (b.similarities_char[a.id] == undefined) {
            a.similarities_char[b.id] = rand.floatBetween(RANDOM_SIMILARITY_MIN, RANDOM_SIMILARITY_MAX);
        }
        else {a.similarities_char[b.id] = b.similarities_char[a.id];}
        
        a.similarities_abi[b.id] = Math.min( Math.max( 1-(Math.abs(a.ability_true-b.ability_true)/(ABILITY_MAX-ABILITY_MIN)),RANDOM_SIMILARITY_MIN), RANDOM_SIMILARITY_MAX);
    }
}

let rows = new Array<any>();
let rows2 = new Array<any>();
let rows_similarities = new Array<any>();
let rows_abilities = new Array<any>();

term.clear();

let ti = 0;
for (let t of tasks) {
    ++ti;
    take_turn(t);
    // term.blue.bold.underline(`------- Task #${i} -------\n`);
    let agent_results = new Array<string>();
    let eff_results = new Array<string>();
    let mood_results = new Array<string>();
    for (let a of agents) {
        let result = a.get_result_of_last_performed_task();
        let resultstr = result ? '✓' : 'x';
        agent_results.push(resultstr);

        let eff = a.ability_est;
        eff_results.push(`${eff.toFixed(0)}`);

        let mood = a.mood;
        mood_results.push(`${mood.toFixed(0)}`);
    }
    rows.push([`${ti}`, ...agent_results]);
    rows2.push([`${ti}`, ...eff_results]);
}

for (let a of agents) {
    let row = new Array<string>();
    for (let b of agents) {
        row.push(`${a.get_similarity_to(b.id).toFixed(2)}`);
    }
    rows_similarities.push([`${a}`, ...row]);
}

for (let a of agents) {
    let row = new Array<string>();
    row.push(a.id, `${a.ability_true}`, `${a.starting_ability_est}`);
    rows_abilities.push(row);
}

term.table([
    ['', ...agents],
    ...rows_similarities
], {
    hasBorder: true,
    contentHasMarkup: true,
    borderChars: 'lightRounded',
    borderAttr: { color: 'blue' },
    textAttr: { bgColor: 'default' },
    firstCellTextAttr: { bgColor: 'green' },
    firstRowTextAttr: { bgColor: 'yellow' },
    firstColumnTextAttr: { bgColor: 'green' },
    width: 60,
    fit: true // Activate all expand/shrink + wordWrap
});

term.table([
    ['#Task', ...agents],
    ...rows
], {
    hasBorder: true,
    contentHasMarkup: true,
    borderChars: 'lightRounded',
    borderAttr: { color: 'blue' },
    textAttr: { bgColor: 'default' },
    firstCellTextAttr: { bgColor: 'green' },
    firstRowTextAttr: { bgColor: 'yellow' },
    firstColumnTextAttr: { bgColor: 'green' },
    width: 60,
    fit: true // Activate all expand/shrink + wordWrap
});

term.table([
    ['#Task', ...agents],
    ...rows2
], {
    hasBorder: true,
    contentHasMarkup: true,
    borderChars: 'lightRounded',
    borderAttr: { color: 'blue' },
    textAttr: { bgColor: 'default' },
    firstCellTextAttr: { bgColor: 'green' },
    firstRowTextAttr: { bgColor: 'yellow' },
    firstColumnTextAttr: { bgColor: 'green' },
    width: 60,
    fit: true // Activate all expand/shrink + wordWrap
});

const records = new Array<any>();
// Prepare output
let agent_headers = [ '#Task', 'Difficulty' ];
for (let a of agents) {
    agent_headers.push(`${a.id} estability`);
}
for (let a of agents) {
    agent_headers.push(`${a.id} result`);
}
for (let a of agents) {
    agent_headers.push(`${a.id} mood`);
}
for (let a of agents) {
    agent_headers.push(`${a.id} effort`);
}
for (let a of agents) {
    agent_headers.push(`${a.id} efficacy`);
}
for (let t of tasks) {
    let row = [];
    row.push(t.id, t.difficulty);
    for (let a of agents) {
        row.push(a.history[t.id - 1].ability_est.toFixed(0));
    }
    for (let a of agents) {
        let history = a.history[t.id - 1];
        let resultstr: string;
        switch (history.result) {
            case true:
                resultstr = 'Success';
            break;
            default: resultstr = 'Fail';
            break;
        }
        if (history.strained) resultstr += ' + strained';
        row.push(resultstr);
    }
    for (let a of agents) {
        row.push(a.history[t.id - 1].mood.toFixed(0));
    }
    for (let a of agents) {
        row.push(a.history[t.id - 1].effort.toFixed(0));
    }
    for (let a of agents) {
        row.push(a.history[t.id - 1].efficacy.toFixed(4));
    }
    records.push(row);
}

let simulation_writer = createCsvWriter({
    path: SIMULATION_OUTPUT_PATH,
    header: [ ...agent_headers ],
    fieldDelimiter: ';',
});

simulation_writer.writeRecords(records)
    .then(() => {
        console.log(`Output written to '${SIMULATION_OUTPUT_PATH}.`);
    });

let similarities_writer = createCsvWriter({
    path: SIMILARITIES_OUTPUT_PATH,
    header: [ '', ...agents ],
    fieldDelimiter: ';',
});

similarities_writer.writeRecords(rows_similarities)
    .then(() => {
        console.log(`Output written to '${SIMILARITIES_OUTPUT_PATH}.`);
    });

let ability_writer = createCsvWriter({
    path: ABILITIES_OUTPUT_PATH,
    header: [ 'Agent', 'True ability', 'Starting efficacy' ],
    fieldDelimiter: ';',
});

ability_writer.writeRecords(rows_abilities)
    .then(() => {
        console.log(`Output written to '${ABILITIES_OUTPUT_PATH}.`);
    });
